#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const SURVEY_TITLE = 'covid-19';

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function exists(selector) {
        await browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
    }

    async function visible(selector) {
        await exists(selector);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function elementExists(selector) {
        let exists;
        try {
            await browser.findElement(selector);
            exists = true;
        } catch (e) {
            exists = false;
        }

        return exists;
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn + '/admin/index.php');
        await visible(By.xpath('//button[text()="Log in"]'));
        await browser.findElement(By.id('user')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Log in"]')).click();

        await browser.sleep(3000);
        if (await elementExists(By.id('welcomeModal'))) {
            await browser.findElement(By.xpath('//div[@id="welcomeModal"]//button[@data-bs-dismiss="modal"]')).click();
            await browser.sleep(1000);
        }

        await browser.wait(until.elementLocated(By.xpath('//a[contains(@href, "/index.php?r=surveyAdministration")]')), TEST_TIMEOUT);
    }

    async function adminLogin(username, password) {
        await browser.get('https://' + app.fqdn + '/admin/index.php');
        await visible(By.xpath('//button[text()="Log in"]'));
        await browser.findElement(By.id('select2-authMethod-container')).click();
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//input[@type="search"]')).sendKeys('i');
        await browser.findElement(By.xpath('//input[@type="search"]')).sendKeys(Key.RETURN);
        await browser.findElement(By.id('user')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Log in"]')).click();

        await browser.sleep(3000);
        if (await elementExists(By.id('welcomeModal'))) {
            await browser.findElement(By.xpath('//div[@id="welcomeModal"]//button[@data-bs-dismiss="modal"]')).click();
            await browser.sleep(1000);
        }

        await browser.wait(until.elementLocated(By.xpath('//a[@href="/index.php?r=admin/globalsettings"]')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/index.php?r=admin/authentication/sa/logout');
        await browser.sleep(3000);
    }

    async function createSurvey() {
        await browser.get('https://' + app.fqdn + '/index.php?r=surveyAdministration/newsurvey');
        await browser.wait(until.elementLocated(By.id('surveyTitle')), TEST_TIMEOUT);
        await browser.findElement(By.id('surveyTitle')).sendKeys(SURVEY_TITLE);
        await browser.findElement(By.id('create-survey-submit')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Question summary")]')), TEST_TIMEOUT);
    }

    async function checkSurvey() {
        await browser.get('https://' + app.fqdn + '/index.php?r=surveyAdministration/listsurveys');
        await browser.wait(until.elementLocated(By.xpath(`//*[text() = "${SURVEY_TITLE}"]`)), TEST_TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', adminLogin.bind(null, 'admin', 'changeme'));
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can create survey', createSurvey);
    it('can check survey', checkSurvey);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', adminLogin.bind(null, 'admin', 'changeme'));
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can check survey', checkSurvey);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        await browser.manage().deleteAllCookies();

        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can admin login', adminLogin.bind(null, 'admin', 'changeme'));
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can check survey', checkSurvey);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // No SSO
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login (no sso)', login.bind(null, 'admin', 'changeme')); // not admin login, since no drop-down
    it('can create survey', createSurvey);
    it('can check survey', checkSurvey);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id org.limesurvey.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can create survey', createSurvey);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can check survey', checkSurvey);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
