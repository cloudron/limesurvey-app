<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
return array(
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=mysql;port=' . getenv('CLOUDRON_MYSQL_PORT') . ';dbname=' . getenv('CLOUDRON_MYSQL_DATABASE') . ';',
            'emulatePrepare' => true,
            'username' => getenv('CLOUDRON_MYSQL_USERNAME'),
            'password' => getenv('CLOUDRON_MYSQL_PASSWORD'),
            'charset' => 'utf8mb4',
            'tablePrefix' => 'lime_',
        ),

        'urlManager' => array(
            'urlFormat' => 'get',
            'rules' => array(
                // You can add your own rules here
            ),
            'showScriptName' => true,
        ),
    ),

    // Use the following config variable to set modified optional settings copied from config-defaults.php
    'config'=>array(
        'debug'=>0,
        'debugsql'=>0, // Set this to 1 to enanble sql logging, only active when debug = 2
    )
);

