FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/limesurvey.conf /etc/apache2/sites-enabled/limesurvey.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod headers expires deflate mime dir rewrite setenvif php8.3

RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/limesurvey/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

# renovate: datasource=github-tags depName=LimeSurvey/LimeSurvey versioning=semver
ARG LIMESURVEY_VERSION=6.11.0+250303

# the is_writable() checks are wrong because they check using access()
RUN curl -L "https://github.com/LimeSurvey/LimeSurvey/archive/$LIMESURVEY_VERSION.tar.gz" | tar -xz --strip-components 1 -f - && \
    sed -i -e "s,is_writable(APPPATH . 'config'),true,g" /app/code/application/controllers/InstallerController.php && \
    sed -i -e "s,is_writable(\$configdir),true,g" /app/code/application/core/LSSodium.php

# security.php contains keys that encrypt the db values
RUN rm -rf /app/code/tmp && ln -s /run/limesurvey/tmp /app/code/tmp && \
    rm -rf /app/code/upload && ln -s /app/data/upload /app/code/upload && \
    ln -s /run/limesurvey/config.php /app/code/application/config/config.php && \
    ln -s /app/data/security.php /app/code/application/config/security.php

# custom command for encrypting passwords (like smtp)
COPY EncryptCommand.php /app/code/application/commands/

RUN chown -R www-data.www-data /app/code

ADD config.php start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
