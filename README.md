# LimeSurvey Cloudron App

This repository contains the Cloudron app package source for [LimeSurvey](https://www.limesurvey.org).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.limesurvey.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.limesurvey.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd limesurvey-app
cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/).

```
cd limesurvey-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha test.js
```
