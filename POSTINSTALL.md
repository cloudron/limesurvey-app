This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the admin password immediately.

The admin panel is available at `/admin/index.php`.
