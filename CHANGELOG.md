[0.0.1]
* Initial release

[0.1.0]
* Improve post install message

[1.0.0]
* Update LimeSurvey to 4.1.16+200407

[1.1.0]
* Update LimeSurvey to 4.1.17+200414

[1.2.0]
* Update LimeSurvey to 4.1.18+200416
* Use latest base image 2.0

[1.3.0]
* Update LimeSurvey to 4.2.2+200504

[1.4.0]
* Update LimeSurvey to 4.2.3+200511

[1.5.0]
* Update LimeSurvey to 4.2.4+200520

[1.6.0]
* Update LimeSurvey to 4.2.5+200526

[1.7.0]
* Update LimeSurvey to 4.2.6+200602

[1.8.0]
* Update LimeSurvey to 4.2.7+200604

[1.8.1]
* Update LimeSurvey to 4.2.8+200608

[1.9.0]
* Update LimeSurvey to 4.3.0+200616

[1.10.0]
* Update LimeSurvey to 4.3.1+200623
* Update tags, forumUrl, screenshots in manifest
* Make php.ini customizable via `/app/data/php.ini`

[1.10.1]
* Update LimeSurvey to 4.3.2+200629

[1.10.2]
* Update LimeSurvey to 4.3.3+200707

[1.11.0]
* Update LimeSurvey to 4.3.4+200713

[1.12.0]
* Update LimeSurvey to 4.3.5+200721

[1.13.0]
* Update LimeSurvey to 4.3.6+200727

[1.14.0]
* Update LimeSurvey to 4.3.7+200730

[1.15.0]
* Update LimeSurvey to 4.3.8+200803

[1.16.0]
* Update LimeSurvey to 4.3.9+200806

[1.17.0]
* Update LimeSurvey to 4.3.10+200812

[1.18.0]
* Update LimeSurvey to 4.3.11+200817

[1.18.1]
* Update LimeSurvey to 4.3.12+200820

[1.18.2]
* Update LimeSurvey to 4.3.13+200824

[1.19.0]
* Update LimeSurvey to 4.3.15+200907

[1.20.0]
* Update LimeSurvey to 4.3.16+200915

[1.20.1]
* Update LimeSurvey to 4.3.17+200921

[1.20.2]
* Update LimeSurvey to 4.3.18+200928

[1.20.3]
* Update LimeSurvey to 4.3.19+201005

[1.20.4]
* Update LimeSurvey to 4.3.20+201012

[1.20.5]
* Update LimeSurvey to 4.3.21+201015

[1.20.6]
* Update LimeSurvey to 4.3.22+201019

[1.21.0]
* Update PHP to 7.4

[1.21.1]
* Update LimeSurvey to 4.3.23+201026

[1.21.2]
* Update LimeSurvey to 4.3.24+201102

[1.21.3]
* Update LimeSurvey to 4.3.25+201105
* [Full changelog](https://github.com/LimeSurvey/LimeSurvey/releases/tag/4.3.25+201105)

[1.21.4]
* Update LimeSurvery to 4.3.26+201110
* [Full changelog](https://github.com/LimeSurvey/LimeSurvey/releases/tag/4.3.26+201110)

[1.21.5]
* Update LimeSurvey to 4.3.27+201116
* [Full changelog](https://github.com/LimeSurvey/LimeSurvey/compare/4.3.27+201116...master)

[1.21.6]
* Update LimeSurvey to 4.3.28+201123

[1.22.0]
* Update Limesurvey to 4.3.29+201130

[1.22.1]
* Update LimeSurvey to 4.3.30+201207
* [Full changelog](https://github.com/LimeSurvey/LimeSurvey/releases/tag/4.3.30+201207)

[1.22.2]
* Update LimeSurvey to 4.3.31+201214
* [Full changelog](https://github.com/LimeSurvey/LimeSurvey/releases/tag/4.3.31+201214)

[1.22.3]
* Update LimeSurvey frontend to 4.3.32+201221
* [Full changelog](https://github.com/LimeSurvey/LimeSurvey/releases/tag/4.3.32+201221)

[1.23.0]
* Update LimeSurvey to 4.3.34+210119

[1.24.0]
* Update LimeSurvey to 4.4.0+210129

[1.24.1]
* Update LimeSurvey to 4.4.3+210209

[1.24.2]
* Use LimeSurvey to 4.4.5+210213

[1.24.3]
* Update LimeSurvey to 4.4.6+210214

[1.24.4]
* Update LimeSurvey to 4.4.7+210215

[1.24.5]
* Update LimeSurvey to 4.4.8+210217

[1.24.6]
* Update LimeSurvery to 4.4.9+210219

[1.24.7]
* Update LimeSurvey to 4.4.10+210222

[1.24.8]
* Update LimeSurvey to 4.4.11+210301

[1.24.9]
* Update LimeSurvey to 4.4.12+210308

[1.24.10]
* Update LimeSurvey to 4.4.13+210315

[1.24.11]
* Update LimeSurvey to 4.4.14

[1.24.12]
* Update LimeSurvey to 4.4.15

[1.24.13]
* Update LimeSurvey to 4.4.16

[1.25.0]
* Update LimeSurvey to 4.5.0+210412

[1.25.1]
* Update LimeSurvey to 4.5.1+210420

[1.25.2]
* Update LimeSurvey to 4.5.2+210426

[1.26.0]
* Update LimeSurvey to 4.6.1+210510

[1.26.1]
* Update LimeSurvey to 4.6.2+210512

[1.26.2]
* Update LimeSurvey to 4.6.3+210518

[1.27.0]
* Update LimeSurvey to 5.0.0+210526

[1.27.1]
* Update LimeSurvey to 5.0.1+210532

[1.27.2]
* Update LimeSurvey to 5.0.2+210607

[1.27.3]
* Update LimeSurvey to 5.0.3+210609

[1.27.4]
* Update LimeSurvey to 5.0.4+210614

[1.27.5]
* Update LimeSurvey to 5.0.5+210621

[1.27.6]
* Update LimeSurvey to 5.0.6+210625

[1.27.7]
* Update LimeSurvey to 5.0.8+210712

[1.27.8]
* Update LimeSurvey to 5.0.9+210722

[1.27.9]
* Update LimeSurvey to 5.0.10+210723

[1.27.10]
* Update LimeSurvey to 5.0.11+210727

[1.28.0]
* Update LimeSurvey to 5.0.12+210729

[1.29.0]
* Update LimeSurvey to 5.0.13

[1.30.0]
* Update LimeSurvey to 5.1.0

[1.30.1]
* Update LimeSurvey to 5.1.1

[1.30.2]
* Update LimeSurvey to 5.1.2

[1.30.3]
* Update LimeSurvey to 5.1.3

[1.30.4]
* Update LimeSurvey to 5.1.8

[1.30.5]
* Update LimeSurvey to 5.1.9

[1.30.6]
* Update LimeSurvery to 5.1.11

[1.30.7]
* Update LimeSurvey to 5.1.12

[1.30.8]
* Update LimeSurvey to 5.1.13

[1.30.9]
* Update LimeSurvey to 5.1.15

[1.30.10]
* Update LimeSurvey to 5.1.16

[1.30.11]
* Update LimeSurvey to 5.1.17

[1.30.12]
* Update LimeSurvey to 5.1.18

[1.31.0]
* Update LimeSurvey to 5.2.0

[1.31.1]
* Update LimeSurvey to 5.2.1

[1.31.2]
* Update LimeSurvey to 5.2.2

[1.31.3]
* Update LimeSurvey to 5.2.3

[1.31.4]
* Update LimeSurvey to 5.2.4

[1.31.5]
* Update LimeSurvey to 5.2.5

[1.31.6]
* Update LimeSurvey to 5.2.6

[1.31.7]
* Update LimeSurvey to 5.2.7

[1.31.8]
* Update LimeSurvey to 5.2.9

[1.31.9]
* Update LimeSurvey to 5.2.10

[1.31.10]
* Update LimeSurvey to 5.2.11

[1.31.11]
* Update LimeSurvey to 5.2.12

[1.31.12]
* Update LimeSurvey to 5.2.13

[1.32.0]
* Update LimeSurvey to 5.3.0

[1.32.1]
* Update LimeSurvey to 5.3.1

[1.32.2]
* Update LimeSurvey to 5.3.2

[1.32.3]
* Update LimeSurvey to 5.3.3

[1.32.4]
* Update LimeSurvey to 5.3.4

[1.32.5]
* Update LimeSurvey to 5.3.5

[1.32.6]
* Update LimeSurvey to 5.3.6

[1.32.7]
* Update LimeSurvey to 5.3.7

[1.32.8]
* Update LimeSurvey to 5.3.8

[1.32.9]
* Update LimeSurvey to 5.3.9

[1.32.10]
* Update LimeSurvey to 5.3.10

[1.32.11]
* Update LimeSurvey to 5.3.11

[1.32.12]
* Update LimeSurvey to 5.3.12

[1.32.13]
* Update LimeSurvey to 5.3.13

[1.32.14]
* Update LimeSurvey to 5.3.14

[1.32.15]
* Update LimeSurvey to 5.3.15

[1.32.16]
* Update LimeSurvey to 5.3.16

[1.32.17]
* Update LimeSurvey to 5.3.17

[1.32.18]
* Update LimeSurvey to 5.3.18

[1.33.0]
* Update LimeSurvey to 5.3.19

[1.33.1]
* Update LimeSurvey to 5.3.20

[1.33.2]
* Update LimeSurvey to 5.3.21

[1.33.3]
* Update LimeSurvey to 5.3.22

[1.33.4]
* Update LimeSurvey to 5.3.23

[1.33.5]
* Update LimeSurvey to 5.3.25

[1.33.6]
* Update LimeSurvey to 5.3.26

[1.33.7]
* Update LimeSurvey to 5.3.27

[1.33.8]
* Update LimeSurvey to 5.3.28

[1.33.9]
* Update LimeSurvey to 5.3.29

[1.33.10]
* Update LimeSurvey to 5.3.30

[1.33.11]
* Update LimeSurvey to 5.3.31

[1.34.0]
* Update LimeSurvey to 5.4.0

[1.34.1]
* Update LimeSurvey to 5.4.1

[1.34.2]
* Update LimeSurvey to 5.4.2

[1.34.3]
* Update LimeSurvey to 5.4.3

[1.34.4]
* Update LimeSurvey to 5.4.4

[1.34.5]
* Update LimeSurvey to 5.4.6

[1.34.6]
* Update LimeSurvey to 5.4.7

[1.34.7]
* Update LimeSurvey to 5.4.8

[1.34.8]
* Update LimeSurvey to 5.4.9

[1.34.9]
* Update LimeSurvey to 5.4.10

[1.34.10]
* Update LimeSurvey to 5.4.11

[1.34.11]
* Update LimeSurvey to 5.4.12

[1.34.12]
* Update LimeSurvey to 5.4.13
* Update to Cloudron base image 4.0.0

[1.34.13]
* Update LimeSurvey to 5.4.14

[1.34.14]
* Update LimeSurvey to 5.4.15

[1.35.0]
* Update LimeSurvey to 5.5.1

[1.35.1]
* Update LimeSurvey to 5.5.2

[1.36.0]
* Update LimeSurvey to 5.6.0

[1.36.1]
* Update LimeSurvey to 5.6.1+230123

[1.36.2]
* Update LimeSurvey to 5.6.2

[1.36.3]
* Update LimeSurvey to 5.6.3

[1.36.4]
* Update LimeSurvey to 5.6.4+230206

[1.36.5]
* Update LimeSurvey to 5.6.5+230214

[1.36.6]
* Update LimeSurvey to 5.6.6

[1.36.7]
* Update LimeSurvey to 5.6.7

[1.36.8]
* Update LimeSurvey to 5.6.8

[1.36.9]
* Update LimeSurvey to 5.6.9

[1.36.10]
* Update LimeSurvey to 5.6.10

[1.36.11]
* Update LimeSurvey to 5.6.12

[1.36.12]
* Update LimeSurvey to 5.6.13

[1.36.13]
* Update LimeSurvey to 5.6.14

[1.37.0]
* Update LimeSurvey to 6.0.0

[1.37.1]
* Update LimeSurvey to 6.0.1
* [Full changelog](https://manual.limesurvey.org/LimeSurvey_roadmap#LimeSurvey_6.0)

[1.37.2]
* Update LimeSurvey to 6.0.2

[1.37.3]
* Update LimeSurvey to 6.0.3

[1.37.4]
* Update LimeSurvey to 6.0.4

[1.37.5]
* Update LimeSurvey to 6.0.5

[1.37.6]
* Update LimeSurvey to 6.0.6

[1.37.7]
* Update LimeSurvey to 6.0.7

[1.38.0]
* Update LimeSurvey to 6.1.0

[1.38.1]
* Update LimeSurvey to 6.1.1

[1.38.2]
* Update LimeSurvey to 6.1.2

[1.38.3]
* Update LimeSurvey to 6.1.3

[1.38.4]
* Update LimeSurvey to 6.1.4

[1.38.5]
* Update LimeSurvey to 6.1.5

[1.38.6]
* Update LimeSurvey to 6.1.6

[1.38.7]
* Update LimeSurvey to 6.1.7

[1.39.0]
* Update LimeSurvey to 6.2.0

[1.39.1]
* Update LimeSurvey to 6.2.1

[1.39.2]
* Update LimeSurvey to 6.2.2

[1.39.3]
* Update LimeSurvey to 6.2.3

[1.39.4]
* Update LimeSurvey to 6.2.4

[1.39.5]
* Update LimeSurvey to 6.2.5

[1.39.6]
* Update LimeSurvey to 6.2.7

[1.39.7]
* Update LimeSurvey to 6.2.8

[1.39.8]
* Update LimeSurvey to 6.2.9
* Update base image to 4.2.0

[1.39.9]
* Update LimeSurvey to 6.2.10

[1.39.10]
* Update LimeSurvey to 6.2.11

[1.40.0]
* Update LimeSurvey to 6.3.0

[1.40.1]
* Update LimeSurvey to 6.3.1

[1.40.2]
* Update LimeSurvey to 6.3.2

[1.40.3]
* Update LimeSurvey to 6.3.3

[1.40.4]
* Update LimeSurvey to 6.3.4

[1.40.5]
* Update LimeSurvey to 6.3.5

[1.40.6]
* Update LimeSurvey to 6.3.6

[1.40.7]
* Update LimeSurvey to 6.3.7

[1.40.8]
* Update LimeSurvey to 6.3.8

[1.40.9]
* Update LimeSurvey to 6.3.9

[1.41.0]
* Update LimeSurvey to 6.4.2

[1.41.1]
* Update LimeSurvey to 6.4.3

[1.41.2]
* Update LimeSurvey to 6.4.4

[1.41.3]
* Update LimeSurvey to 6.4.5

[1.41.4]
* Update LimeSurvey to 6.4.6

[1.41.5]
* Update LimeSurvey to 6.4.7

[1.41.6]
* Update LimeSurvey to 6.4.8

[1.41.7]
* Update LimeSurvey to 6.4.9

[1.41.8]
* Update LimeSurvey to 6.4.10

[1.41.9]
* Update LimeSurvey to 6.4.11

[1.41.10]
* Update LimeSurvey to 6.4.12

[1.42.0]
* Update LimeSurvey to 6.5.0

[1.42.1]
* Update LimeSurvey to 6.5.3

[1.42.2]
* Update LimeSurvey to 6.5.4

[1.42.3]
* Update LimeSurvey to 6.5.5

[1.42.4]
* Update LimeSurvey to 6.5.6

[1.42.5]
* Update LimeSurvey to 6.5.7

[1.42.6]
* Update LimeSurvey to 6.5.8

[1.42.7]
* Update LimeSurvey to 6.5.10

[1.42.8]
* Update LimeSurvey to 6.5.11

[1.42.9]
* Update LimeSurvey to 6.5.12

[1.42.10]
* Update LimeSurvey to 6.5.13

[1.42.11]
* Update LimeSurvey to 6.5.14

[1.42.12]
* Update LimeSurvey to 6.5.15

[1.42.13]
* Update LimeSurvey to 6.5.16

[1.42.14]
* Update LimeSurvey to 6.5.17

[1.42.15]
* Update LimeSurvey to 6.5.18

[1.43.0]
* Update LimeSurvey to 6.6.0

[1.43.1]
* Update LimeSurvey to 6.6.4

[1.43.2]
* Update LimeSurvey to 6.6.5


[1.43.3]
* Update LimeSurvey CE to 6.6.7
[1.43.4]
* Update LimeSurvey to 6.6.8+241104
* [Full Changelog](https://manual.limesurvey.org/LimeSurvey_roadmap#LimeSurvey_6.0)

[1.44.0]
* Update LimeSurvey to 6.8.0+241119
* [Full Changelog](https://manual.limesurvey.org/LimeSurvey_roadmap#LimeSurvey_6.0)

[1.44.1]
* Update LimeSurvey to 6.8.1+241120
* [Full Changelog](https://manual.limesurvey.org/LimeSurvey_roadmap#LimeSurvey_6.0)

[1.44.2]
* Update LimeSurvey to 6.8.2+241203
* [Full Changelog](https://manual.limesurvey.org/LimeSurvey_roadmap#LimeSurvey_6.0)

[1.45.0]
* Update LimeSurvey to 6.9.0+241218

[1.46.0]
* Update LimeSurvey to 6.10.0+250106

[1.46.1]
* Update LimeSurvey to 6.10.1

[1.46.2]
* Update LimeSurvey to 6.10.2+250127

[1.46.3]
* Update LimeSurvey to 6.10.3+250203

[1.46.4]
* Update LimeSurvey to 6.10.4+250210

[1.46.5]
* Update LimeSurvey to 6.10.5+250217

[1.46.6]
* Update LimeSurvey to 6.10.6+250224

[1.47.0]
* Update base image to 5.0.0

[1.48.0]
* Update LimeSurvey to 6.11.0+250303

