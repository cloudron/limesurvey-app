#!/bin/bash

set -eu

export MYSQL_PWD=${CLOUDRON_MYSQL_PASSWORD} # This will avoid the mysql password warning
mysql="mysql --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE}"

# Do not use tmp directly due to tmpreaper removeing built assets
mkdir -p /run/limesurvey/sessions /run/limesurvey/tmp/runtime /run/limesurvey/tmp/assets /run/limesurvey/tmp/upload

cp /app/pkg/config.php /run/limesurvey/config.php

echo "==> Ensure folders"
# It is not exactly clear why the app does not create those subfolders and we might miss some here
mkdir -p /app/data/upload /app/code/upload/surveys /app/code/upload/admintheme /app/code/upload/themes/survey/generalfiles

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "==> Changing ownership"
chown -R www-data.www-data /run/limesurvey /app/data

if [[ ! -f "/app/data/security.php" ]]; then
    echo "==> Run installation script"
    sudo -E -u www-data php /app/code/application/commands/console.php install admin changeme Administrator admin@server.local verbose
    $mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('siteadminname', 'Administrator')"
    # should this be done all the time?
    $mysql -e "UPDATE lime_surveys_groupsettings SET adminemail='${CLOUDRON_MAIL_FROM}' WHERE owner_id=1"
fi

# db settings take precedence over config.php
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('force_ssl', 'on')"

echo "==> Configure email"
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('siteadminemail', '${CLOUDRON_MAIL_FROM}')"
# support for single quote in splace name
display_name=$(echo -n "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-LimeSurvey}" | base64)
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('siteadminname', FROM_BASE64('${display_name}'))"
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('siteadminbounce', '${CLOUDRON_MAIL_FROM}')"
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('emailmethod', 'smtp')"
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('emailsmtpssl', '')"
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('emailsmtphost', '${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}')"
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('emailsmtpuser', '${CLOUDRON_MAIL_SMTP_USERNAME}')"

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    echo "==> Configure LDAP plugin"
    ldap_plugin_id=$($mysql -Bs -e "SELECT id FROM lime_plugins WHERE name='AuthLDAP'")
    $mysql -e "UPDATE lime_plugins SET active=1 WHERE id=${ldap_plugin_id}"

    # can't do normal upserts, since the primary is the id column, which we don't know upfront
    declare -A ldap_keys;
    declare -A ldap_values;
    ldap_keys[0]="server";                     ldap_values[${ldap_keys[0]}]="'\"${CLOUDRON_LDAP_SERVER}\"'"
    ldap_keys[1]="ldapport";                   ldap_values[${ldap_keys[1]}]="'\"${CLOUDRON_LDAP_PORT}\"'"
    ldap_keys[2]="ldapversion";                ldap_values[${ldap_keys[2]}]="'\"2\"'"
    ldap_keys[3]="ldapoptreferrals";           ldap_values[${ldap_keys[3]}]="'\"0\"'"
    ldap_keys[4]="ldaptls";                    ldap_values[${ldap_keys[4]}]="'null'"
    ldap_keys[5]="ldapmode";                   ldap_values[${ldap_keys[5]}]="'\"searchandbind\"'"
    ldap_keys[6]="userprefix";                 ldap_values[${ldap_keys[6]}]="'null'"
    ldap_keys[7]="domainsuffix";               ldap_values[${ldap_keys[7]}]="'null'"
    ldap_keys[8]="searchuserattribute";        ldap_values[${ldap_keys[8]}]="'\"username\"'"
    ldap_keys[9]="usersearchbase";             ldap_values[${ldap_keys[9]}]="'\"${CLOUDRON_LDAP_USERS_BASE_DN}\"'"
    ldap_keys[10]="extrauserfilter";           ldap_values[${ldap_keys[10]}]="'\"\"'"
    ldap_keys[11]="binddn";                    ldap_values[${ldap_keys[11]}]="'\"${CLOUDRON_LDAP_BIND_DN}\"'"
    ldap_keys[12]="bindpwd";                   ldap_values[${ldap_keys[12]}]="'\"${CLOUDRON_LDAP_BIND_PASSWORD}\"'"
    ldap_keys[13]="mailattribute";             ldap_values[${ldap_keys[13]}]="'\"mail\"'"
    ldap_keys[14]="fullnameattribute";         ldap_values[${ldap_keys[14]}]="'\"displayname\"'"
    ldap_keys[15]="is_default";                ldap_values[${ldap_keys[15]}]="'\"1\"'"
    ldap_keys[16]="autocreate";                ldap_values[${ldap_keys[16]}]="'\"1\"'"
    ldap_keys[17]="automaticsurveycreation";   ldap_values[${ldap_keys[17]}]="'\"1\"'"
    ldap_keys[18]="groupsearchbase";           ldap_values[${ldap_keys[18]}]="'\"\"'"
    ldap_keys[19]="groupsearchfilter";         ldap_values[${ldap_keys[19]}]="'\"\"'"
    ldap_keys[20]="allowInitialUser";          ldap_values[${ldap_keys[20]}]="'\"1\"'"

    for key in ${ldap_keys[@]}; do
        if [[ -z `$mysql -e "SELECT * FROM lime_plugin_settings WHERE plugin_id=${ldap_plugin_id} AND lime_plugin_settings.key='${key}';"` ]]; then
            echo "  ==> Insert new ldap config ${key} = ${ldap_values[$key]}"
            $mysql -e "INSERT INTO lime_plugin_settings (plugin_id, lime_plugin_settings.key, value) VALUES (${ldap_plugin_id}, '${key}', ${ldap_values[$key]});"
        else
            echo "  ==> Update ldap config ${key} = ${ldap_values[$key]}"
            $mysql -e "UPDATE lime_plugin_settings SET value=${ldap_values[$key]} WHERE plugin_id=${ldap_plugin_id} AND lime_plugin_settings.key='${key}';"
        fi
    done
fi

echo "==> Run database schema update"
# there is a bug in migration 452 where it tries to decryptOld the smtp password. i think it should just be decrypt(). see https://github.com/LimeSurvey/LimeSurvey/commit/14f84f345c5eb58aa83fb0428991d4730c610eed
$mysql -e "DELETE FROM lime_settings_global WHERE stg_name='emailsmtppassword'"
sudo -E -u www-data php /app/code/application/commands/console.php updatedb

encrypted_password=$(sudo -E -u www-data php /app/code/application/commands/console.php encrypt "${CLOUDRON_MAIL_SMTP_PASSWORD}")
$mysql -e "REPLACE INTO lime_settings_global (stg_name, stg_value) VALUES ('emailsmtppassword', '${encrypted_password}')"

echo "==> Start apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
