### Overview

LimeSurvey is the tool to use for your online surveys. Whether you are conducting simple
questionnaires with just a couple of questions or advanced assessments with conditionals
and quota management, LimeSurvey has got you covered.

LimeSurvey is 100% open source and will always be transparently developed.
We can help you reach your goals.

### Features

* Multilingual - Supporting more than 80 languages
* Unlimited administrators - Give administrative access to colleagues or friends
* 28 different question types - We have them all!
* What you see is what you get - Simple editing of your HTML content
* Quota management - Control your target group participation by quotas
* Skip logic / branching - Set conditions for your questions based on previous answers
* Reuse survey content - Copy questions, answers or complete surveys
* Analyze your surveys easier - Use assessment scores to analyze survey site.
* Invite and remind - Invite your participant using LimeSurvey
* Anonymized responses - protected participants - Invite participants by email and anonymize their responses
* Open and closed surveys - Create invite-only, open or public surveys
* Design your survey appearance - Individual design of your survey
* Offline functionality - Printable version and manual data entry
* Visualize your response data - Create statistics and graphs inside the application
* Export your responses - Export to all common formats and applications
* Dynamic questions - Create questions based on previous answers

